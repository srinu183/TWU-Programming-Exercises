package com.tw;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.tw.ConsoleIO.LINE_SEPARATOR;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TriangleExerciseTest {

    private TriangleExercise triangleExercise;
    private IO io;

    @BeforeEach
    void setUp() {
        this.io = mock(IO.class);
        this.triangleExercise = new TriangleExercise(this.io);
    }

    @Test
    void drawAnAsterisk() {
        String asterisk = "*";
        this.triangleExercise.drawAsterisk();

        verify(this.io).print(asterisk);
    }

    @Test
    void drawAHorizontalLineWithTwoAsterisks() {
        String horizontalLineWithTwoAsterisks = "**";

        this.triangleExercise.drawHorizontalLine(2);

        verify(this.io).print(horizontalLineWithTwoAsterisks);
    }

    @Test
    void drawAHorizontalLineWithThreeAsterisks() {
        String horizontalLineWithThreeAsterisks = "***";

        this.triangleExercise.drawHorizontalLine(3);

        verify(this.io).print(horizontalLineWithThreeAsterisks);
    }

    @Test
    void drawAVerticalLineWithTwoAsterisks() {
        String horizontalLineWithTwoAsterisks = "*" + LINE_SEPARATOR + "*" + LINE_SEPARATOR;

        this.triangleExercise.drawAVerticalLine(2);

        verify(this.io).print(horizontalLineWithTwoAsterisks);
    }

    @Test
    void drawVerticalLineWithThreeAsterisks() {
        String horizontalLineWithTwoAsterisks = "*" + LINE_SEPARATOR + "*" + LINE_SEPARATOR + "*" + LINE_SEPARATOR;

        this.triangleExercise.drawAVerticalLine(3);

        verify(this.io).print(horizontalLineWithTwoAsterisks);
    }

    @Test
    void drawARightTriangleWithTwoLines() {
        String rightTriangle = "*" + LINE_SEPARATOR + "**" + LINE_SEPARATOR;

        this.triangleExercise.drawRightTriangle(2);

        verify(this.io).print(rightTriangle);
    }

    @Test
    void drawARightTriangleWithThreeLines() {
        String rightTriangle = "*" + LINE_SEPARATOR + "**" + LINE_SEPARATOR + "***" + LINE_SEPARATOR;

        this.triangleExercise.drawRightTriangle(3);

        verify(this.io).print(rightTriangle);
    }

}
