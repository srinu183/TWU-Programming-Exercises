package com.tw;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PrimeFactorsGeneratorTest {

    private PrimeFactorsGenerator primeFactorsgenerator;

    @BeforeEach
    void setUp() {
        this.primeFactorsgenerator = new PrimeFactorsGenerator();
    }

    @Test
    void shouldReturnPrimeFactorsOfTwo() {
        Integer[] primeFactorsOfTwo = {2};

        Assertions.assertArrayEquals(primeFactorsOfTwo, this.primeFactorsgenerator.generate(2).toArray());
    }

    @Test
    void shouldReturnPrimeFactorsOfThirty() {
        Integer[] primeFactorsOfThirty = {2, 3, 5};

        Assertions.assertArrayEquals(primeFactorsOfThirty, this.primeFactorsgenerator.generate(30).toArray());
    }

}
