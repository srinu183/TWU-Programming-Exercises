package com.tw;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.PrintStream;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class ConsoleIOTest {

    private PrintStream out;
    private ConsoleIO consoleIO;

    @BeforeEach
    void setUp() {
        this.out = mock(System.out.getClass());
        this.consoleIO = new ConsoleIO(this.out);
    }

    @Test
    void shouldDisplayHai() {
        String hai = "hai";
        this.consoleIO.print(hai);

        verify(this.out).print(hai);
    }

    @Test
    void shouldDisplayHello() {
        String hello = "hello";
        this.consoleIO.print(hello);

        verify(this.out).print(hello);
    }

}
