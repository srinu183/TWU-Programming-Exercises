package com.tw;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.tw.ConsoleIO.LINE_SEPARATOR;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class DiamondExerciseTest {

    private DiamondExercise diamondExercise;
    private IO io;

    @BeforeEach
    void setUp() {
        this.io = mock(IO.class);
        this.diamondExercise = new DiamondExercise(this.io);
    }

    @Test
    void shouldDisplayAIsoscelesTriangleFacingUpwardWithTwoLines() {
        String isoscelesTriangle = " *"
                + LINE_SEPARATOR + "***"
                + LINE_SEPARATOR;
        this.diamondExercise.drawIsoscelesTriangleFacingUpward(2);

        verify(this.io).print(isoscelesTriangle);
    }

    @Test
    void shouldDisplayAIsoscelesTriangleFacingUpwardWithThreeLines() {
        String isoscelesTriangle = "  *"
                + LINE_SEPARATOR + " ***"
                + LINE_SEPARATOR + "*****" + LINE_SEPARATOR;
        this.diamondExercise.drawIsoscelesTriangleFacingUpward(3);

        verify(this.io).print(isoscelesTriangle);
    }

    @Test
    void shouldDisplayDiamondWithTwoLines() {
        String diamond = " *"
                + LINE_SEPARATOR + "***"
                + LINE_SEPARATOR + " *"
                + LINE_SEPARATOR;
        this.diamondExercise.drawDiamond(2);
        verify(this.io).print(diamond);
    }

    @Test
    void shouldDisplayDiamondWithThreeLines() {
        String diamond = "  *"
                + LINE_SEPARATOR + " ***"
                + LINE_SEPARATOR + "*****"
                + LINE_SEPARATOR + " ***"
                + LINE_SEPARATOR + "  *"
                + LINE_SEPARATOR;
        this.diamondExercise.drawDiamond(3);
        verify(this.io).print(diamond);
    }

    @Test
    void shouldDisplayDiamondWithNameWithTwoLines() {
        String name = "name";
        String diamond = " *"
                + LINE_SEPARATOR + name
                + LINE_SEPARATOR + " *"
                + LINE_SEPARATOR;
        this.diamondExercise.drawDiamondWithName(name, 2);
        verify(this.io).print(diamond);
    }

    @Test
    void shouldDisplayDiamondWithWithThreeLines() {
        String name = "name";
        String diamond = "  *"
                + LINE_SEPARATOR + " ***"
                + LINE_SEPARATOR + name
                + LINE_SEPARATOR + " ***"
                + LINE_SEPARATOR + "  *"
                + LINE_SEPARATOR;
        this.diamondExercise.drawDiamondWithName(name, 3);
        verify(this.io).print(diamond);
    }

}
