package com.tw;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FizzBuzzTest {

    FizzBuzz fizzBuzz;

    @BeforeEach
    void setUp() {
        this.fizzBuzz = new FizzBuzz();
    }

    @Test
    void shouldReturnGivenNumber() {
        int number = 1;

        assertEquals(String.valueOf(number), this.fizzBuzz.findFizzBuzz(number));
    }

    @Test
    void shouldReturnFizzIfNumberIsMultipleOfThree() {
        int multipleOfThree = 9;

        assertEquals("Fizz", this.fizzBuzz.findFizzBuzz(multipleOfThree));
    }

    @Test
    void shouldReturnBuzzIfNumberIsMultipleOfFive() {
        int multipleOfFive = 10;

        assertEquals("Buzz", this.fizzBuzz.findFizzBuzz(multipleOfFive));
    }

    @Test
    void shouldReturnFizzBuzzIfNumberIsMultipleOfBothThreeAndFive() {
        int multipleOfFifteen = 15;

        assertEquals("FizzBuzz", this.fizzBuzz.findFizzBuzz(multipleOfFifteen));
    }

}