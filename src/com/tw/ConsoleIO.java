package com.tw;

import java.io.PrintStream;

public class ConsoleIO implements IO {

    public static final String LINE_SEPARATOR = "\n";

    private final PrintStream out;

    public ConsoleIO(PrintStream out) {
        this.out = out;
    }

    @Override
    public void print(String message) {
        this.out.print(message);
    }

}
