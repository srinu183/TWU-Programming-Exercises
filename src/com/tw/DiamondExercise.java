package com.tw;

import static com.tw.ConsoleIO.LINE_SEPARATOR;

public class DiamondExercise {

    private static final int OFFSET = 1;
    private static final int MULTIPLIER = 2;
    private static final int INITIAL_NUMBER = 0;
    private static final String SPACE = " ";
    private static final String ASTERISK = "*";

    private final IO io;

    public DiamondExercise(IO io) {
        this.io = io;
    }

    public void drawIsoscelesTriangleFacingUpward(int numberOfLines) {
        int noOfAsterisksInBase = MULTIPLIER * numberOfLines - OFFSET;
        String upperPartOfDiamond = upperPartOfDiamond(numberOfLines - OFFSET);
        String base = horizontalLineWith(ASTERISK, noOfAsterisksInBase);
        this.io.print(upperPartOfDiamond + base + LINE_SEPARATOR);
    }

    public void drawDiamond(int numberOfLines) {
        int noOfAsterisksInBase = MULTIPLIER * numberOfLines - OFFSET;
        String base = horizontalLineWith(ASTERISK, noOfAsterisksInBase);
        this.io.print(diamondWith(base, numberOfLines));
    }

    public void drawDiamondWithName(String name, int numberOfLines) {
        this.io.print(diamondWith(name, numberOfLines));
    }

    private String diamondWith(String base, int numberOfLines) {
        String upperPartOfDiamond = upperPartOfDiamond(numberOfLines - OFFSET);
        String lowerPartOfDiamond = lowerPartOfDiamond(numberOfLines - OFFSET);
        return upperPartOfDiamond + base + LINE_SEPARATOR + lowerPartOfDiamond;
    }

    private String upperPartOfDiamond(int numberOfLines) {
        StringBuilder representation = new StringBuilder();
        for (int lineNumber = INITIAL_NUMBER; lineNumber < numberOfLines; lineNumber += OFFSET) {
            int numberOfSpacesInARow = numberOfLines - lineNumber;
            int numberOfAsterisksInARow = MULTIPLIER * lineNumber + OFFSET;
            representation.append(rowOfDiamondWith(numberOfSpacesInARow, numberOfAsterisksInARow));
        }
        return representation.toString();
    }

    private String lowerPartOfDiamond(int numberOfLines) {
        StringBuilder representation = new StringBuilder();
        for (int lineNumber = INITIAL_NUMBER; lineNumber < numberOfLines; lineNumber += OFFSET) {
            int numberOfAsterisksInARow = MULTIPLIER * (numberOfLines - lineNumber) - OFFSET;
            int numberOfSpacesInARow = lineNumber + OFFSET;
            representation.append(rowOfDiamondWith(numberOfSpacesInARow, numberOfAsterisksInARow));
        }
        return representation.toString();
    }

    private String rowOfDiamondWith(int numberOfSpaces, int numberOfAsterisks) {
        return horizontalLineWith(SPACE, numberOfSpaces) +
                horizontalLineWith(ASTERISK, numberOfAsterisks) +
                LINE_SEPARATOR;
    }

    private String horizontalLineWith(String message, int numberOfTimes) {
        StringBuilder representation = new StringBuilder();
        for (int number = INITIAL_NUMBER; number < numberOfTimes; number += OFFSET) {
            representation.append(message);
        }
        return representation.toString();
    }

}
