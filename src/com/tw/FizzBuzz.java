package com.tw;

public class FizzBuzz {
    private static final String FIZZ = "Fizz";
    private static final String BUZZ = "Buzz";
    private static final String FIZZ_BUZZ = "FizzBuzz";

    public String findFizzBuzz(int number) {
        boolean isMultipleOfThree = number % 3 == 0;
        boolean isMultipleOfFive = number % 5 == 0;
        if (isMultipleOfThree && isMultipleOfFive) {
            return FIZZ_BUZZ;
        }
        if (isMultipleOfThree) {
            return FIZZ;
        }
        if (isMultipleOfFive) {
            return BUZZ;
        }
        return String.valueOf(number);
    }

}
