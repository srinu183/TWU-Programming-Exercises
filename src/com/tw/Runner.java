package com.tw;

public class Runner {

    public static void main(String[] args) {
        executeTriangleExercise();
        executeDiamondExercise();
        generateFizzBuzzNumbers(1, 100);
        generatePrimeFactors(30);
    }

    private static void executeTriangleExercise() {
        System.out.println("------------------------------------------------");
        System.out.println("Triangle Exercises::\n");
        TriangleExercise triangleExercise = new TriangleExercise(new ConsoleIO(System.out));
        System.out.println("Asterisk::");
        triangleExercise.drawAsterisk();
        System.out.println("\n\nHorizontal Line::");
        triangleExercise.drawHorizontalLine(3);
        System.out.println("\n\nVertical Line::");
        triangleExercise.drawAVerticalLine(3);
        System.out.println("\nRightTriangle::");
        triangleExercise.drawRightTriangle(3);
        System.out.println("---------------------------------------------------------------");
    }

    private static void executeDiamondExercise() {
        System.out.println("Diamond Exercises::\n");
        DiamondExercise diamondExercise = new DiamondExercise(new ConsoleIO(System.out));
        System.out.println("Isosceles triangle::\n");
        diamondExercise.drawIsoscelesTriangleFacingUpward(3);
        System.out.println("\nDiamond::\n");
        diamondExercise.drawDiamond(3);
        System.out.println("\nDiamond with given name::\n");
        diamondExercise.drawDiamondWithName("Hello", 3);
        System.out.println();
        System.out.println("----------------------------------------------------------------");
    }

    private static void generateFizzBuzzNumbers(int startingNumber, int endingNumber) {
        System.out.println("Fizz buzz number between : " + startingNumber + " to " + endingNumber);
        FizzBuzz fizzBuzz = new FizzBuzz();
        for (int number = startingNumber; number <= endingNumber; number++) {
            System.out.println(fizzBuzz.findFizzBuzz(number));
        }
        System.out.println("----------------------------------------------------------------");
    }

    private static void generatePrimeFactors(int number) {
        System.out.println("PrimeFactors of " + number + " :");
        PrimeFactorsGenerator primeFactorsGenerator = new PrimeFactorsGenerator();
        for (Integer factor : primeFactorsGenerator.generate(number)) {
            System.out.println(factor);
        }
        System.out.println("----------------------------------------------------------------");
    }

}
