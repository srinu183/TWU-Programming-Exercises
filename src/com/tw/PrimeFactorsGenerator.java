package com.tw;

import java.util.ArrayList;
import java.util.List;

public class PrimeFactorsGenerator {

    private static final int INITIAL_EVEN_FACTOR = 2;
    private static final int OFFSET = 2;
    private static final int ZERO_REMAINDER = 0;
    private static final int INITIAL_PRIME_NUMBER = 2;
    private static final int INITIAL_ODD_FACTOR = 3;

    private final List<Integer> primeFactors;

    public PrimeFactorsGenerator() {
        this.primeFactors = new ArrayList<>();
    }

    public List<Integer> generate(int number) {
        while (numberIsDivisibleByAFactor(number, INITIAL_EVEN_FACTOR)) {
            number /= INITIAL_EVEN_FACTOR;
            primeFactors.add(INITIAL_EVEN_FACTOR);
        }
        for (int factor = INITIAL_ODD_FACTOR; factor <= Math.sqrt(number); factor += OFFSET) {
            while (numberIsDivisibleByAFactor(number, factor)) {
                number /= factor;
                primeFactors.add(factor);
            }
        }
        if (number > INITIAL_PRIME_NUMBER) {
            primeFactors.add(number);
        }
        return primeFactors;
    }

    private boolean numberIsDivisibleByAFactor(int number, int factor) {
        return number % factor == ZERO_REMAINDER;
    }

}
