package com.tw;

import static com.tw.ConsoleIO.LINE_SEPARATOR;

public class TriangleExercise {

    private static final String ASTERISK = "*";
    private static final String EMPTY = "";
    private static final int OFFSET = 1;
    private static final int INITIAL_NUMBER = 1;

    private final IO io;

    public TriangleExercise(IO io) {
        this.io = io;
    }

    public void drawAsterisk() {
        this.io.print(ASTERISK);
    }

    public void drawHorizontalLine(int numberOfAsterisks) {
        String horizontalLine = representationOfHorizontalLine(numberOfAsterisks);
        this.io.print(horizontalLine);
    }

    public void drawAVerticalLine(int numberOfAsterisks) {
        String verticalLine = representationOfVerticalLine(numberOfAsterisks);
        this.io.print(verticalLine);
    }

    public void drawRightTriangle(int numberOfLines) {
        String rightTriangle = representationOfRightTriangleWith(numberOfLines);
        this.io.print(rightTriangle);
    }

    private String representationOfRightTriangleWith(int numberOfLines) {
        StringBuilder rightTriangle = new StringBuilder();
        for (int lineNumber = INITIAL_NUMBER; lineNumber <= numberOfLines; lineNumber += OFFSET) {
            rightTriangle.append(representationOfHorizontalLine(lineNumber));
            rightTriangle.append(LINE_SEPARATOR);
        }
        return rightTriangle.toString();
    }

    private String representationOfVerticalLine(int numberOfAsterisks) {
        StringBuilder verticalLine = new StringBuilder(EMPTY);
        for (int number = INITIAL_NUMBER; number <= numberOfAsterisks; number += OFFSET) {
            verticalLine.append(ASTERISK).append(LINE_SEPARATOR);
        }
        return verticalLine.toString();
    }

    private String representationOfHorizontalLine(int numberOfAsterisks) {
        StringBuilder horizontalLine = new StringBuilder(EMPTY);
        for (int number = INITIAL_NUMBER; number <= numberOfAsterisks; number += OFFSET) {
            horizontalLine.append(ASTERISK);
        }
        return horizontalLine.toString();
    }

}
